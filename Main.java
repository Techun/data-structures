public class Main {
    public static void main(String[] args) {
        List list = new List();
        list.add(5, 0);
        list.add(3, 1);
        list.add(8, 2);
        list.add(1, 3);
        list.add(10, 2);
        list.view();

        list.replace(15, 2);
        list.view();

        list.delete(1);
        list.view();

        //TASK2
        LinkedList List = new LinkedList();
        List.add(1);
        List.add(2);
        List.add(3);
        List.display();
        List.add(4, 1);
        List.replace(3, 5);
        List.delete(2);
        List.display();

    }
}
